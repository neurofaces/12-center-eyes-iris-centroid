function folder_results_max = center_iris_in_image_by_iris_centroid(folder_imgs, extension)
% TODO: center iris in image by iris_centroid
% 2 versions: 
% - one done with minimum height and width from iris to image border
% - another with maximum

pri = priority('h');

% folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\';
%folder_imgs = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\eyes-without-mask\';
% % % folder_results_min = fullfile(folder_imgs, 'eyes-without-mask-iris-aligned-middle-min-size\');
% % % if(~exist(folder_results_min, 'dir')), mkdir(folder_results_min); end;
folder_results_max = fullfile(folder_imgs, 'ewom-iris-aligned-middle-MAX-size\');
if(~exist(folder_results_max, 'dir')), mkdir(folder_results_max); end

% extension = '.jpg';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs '\CFD*' extension]);
n_img_filenames = numel(img_filenames);

% load IRIS_CENTROID
% load('C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\9c-face-parametrization-9b-bad-iris-manually\allparameters.mat');
load('E:\DOCTORADO\01_DATA\9-face-parametrization\9c-face-parametrization-9b-bad-iris-manually\allparameters.mat');
iris_centroids = {allparameters.eyes.iris_centroid}';
iris_centroids = reshape([iris_centroids{:}], 2, numel(iris_centroids))';
filenames = {allparameters.eyes.filename}';

% find sizes of all iris images
% sizes = find_sizes_of_all_images(folder_imgs, img_filenames, extension);

% find distance of each iris from iris' centroid to top of the image
% di2top = sizes(:,1)-iris_centroids(:,1);
% medium_img_height = round(mean(di2top) + 2*std(di2top));

% start the loop for every image in the list to place the iris in the
% center
% hw = waitbar(0, 'Centering images... 0%');
t=tic;
% using parfor makes all images black
for nf=1:n_img_filenames
    
    %waitbar(nf/n_img_filenames, hw, sprintf('Centering images... %.2f%%', nf/n_img_filenames*100));
    %cprintf('blue', ['Centering image ' num2str(nf) '/' num2str(n_img_filenames) '\n']);
    fprintf('Centering image %d/%d\n', nf, n_img_filenames);
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load IRIS image
    img = imread(fullfile(folder_imgs, [filename extension]));
    if(islogical(img)), img = uint8(img)*255; end
    [hi, wi, di] = size(img);
    
    % % Get corresponding IRIS_CENTROID (AND CORRECT IT IF IT IS RIGHT EYE)
    nfile = strcmp(filenames, filename);
    c = iris_centroids(nfile,:);
    if(strfind(filename, '_R'))
        c(2) = wi - c(2);
    end
    
    % create a new image placing the iris in the vertical middle adding the
    % same space on both the top and the bottom side (this size will be the
    % minimum length of: iris-top of the image vs iris-bottom of the image 
    % for one version and the maximum for the another version). And the 
    % same placing the image in the horizontal middle.
    
    % MINIMUM version
%     mindh = min(c(1), hi-c(1));
%     mindw = min(c(2), wi-c(2));
%     % if mindh is the distance from the top of the iris image to the iris 
%     % centroid, then use the same number of pixels from the iris center to 
%     % the bottom of the iris image, and the same with width
%     if(mindh == c(1) && mindw == c(2))
%         resize_from = 't-left';
%     end
%     if(mindh == c(1) && mindw ~= c(2))
%         resize_from = 't-right';
%     end
%     if(mindh ~= c(1) && mindw == c(2))
%         resize_from = 'b-left';
%     end
%     if(mindh ~= c(1) && mindw ~= c(2))
%         resize_from = 'b-right';
%     end
%     resized_img_min = resize_image_canvas(img, [2*mindh 2*mindw], resize_from);
    
    % MAXIMUM version
    maxdh = max(c(1), hi-c(1));
    maxdw = max(c(2), wi-c(2));
    % if maxdh is the distance from the top of the iris image to the iris 
    % centroid, then use the same number of pixels from the iris center to 
    % the bottom of the iris image, and the same with width
    if(maxdh == c(1) && maxdw == c(2))
        resize_from = 't-left';
    end
    if(maxdh == c(1) && maxdw ~= c(2))
        resize_from = 't-right';
    end
    if(maxdh ~= c(1) && maxdw == c(2))
        resize_from = 'b-left';
    end
    if(maxdh ~= c(1) && maxdw ~= c(2))
        resize_from = 'b-right';
    end
    resized_img_max = resize_image_canvas(img, [2*maxdh 2*maxdw], resize_from);
    
    % % Save images
% % %     imwrite(resized_img_min, fullfile(folder_results_min, strcat(filename, '_min.bmp')));
    imwrite(resized_img_max, fullfile(folder_results_max, strcat(filename, '_max', extension)));
    
end
toc(t);
% close(hw);

% NOW, all iris must be aligned in the vertical and horizontal middle of the
% image, but images may be of different size. Then, images need now to be 
% resized to the same size. To do so, "resize_all_images_to_same_size.m" will 
% be used. This script does a canvas resizing with resize_from = center.
% Location of the script: \11a-resizing-canvas-to-same-size-and-to-8bit
